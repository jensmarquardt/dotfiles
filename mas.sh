#!/usr/bin/env bash

# Sign into the Mac App Store
mas signin …

# BetterSnapTool (1.8)
mas install 417375580

# Blackmagic Disk Speed Test (3.1)
mas install 425264550

# Tweetbot (2.5.3)
mas install 557168941

# Todoist (7.0.4)
mas install 585829637

# Slack (2.8.2)
mas install 803453959 

# The Unarchiver (3.11.3)
mas install 425424353

# Sign out from the Mac App Store
mas signout
